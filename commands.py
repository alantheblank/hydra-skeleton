import discord
from discord.ext import commands
from discord import FFmpegPCMAudio
import asyncio
import yt_dlp as ytdl
import os
import json
from spotdl.search.spotify_client import SpotifyClient
from spotdl.search.song_gatherer import from_album, from_spotify_url
from spotdl.download.downloader import DownloadManager
from youtube_search import YoutubeSearch


class Logger(object):
    def debug(self, msg):
        print(msg)
        #pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


class Commands(commands.Cog):
    Queue = []
    song = None

    def __init__(self, bot):
        self.bot = bot
        self.spotify_client = self.init_spotify()
        self.DownloadManager = DownloadManager()

    @commands.command(pass_context=True)
    async def ping(self, ctx):
        await ctx.send("Pong")

    @commands.command(pass_context=True)
    async def join(self, ctx):
        if ctx.author.voice:
            channel = ctx.author.voice.channel
            await channel.connect()
            await ctx.guild.change_voice_state(channel=channel, self_deaf=True)
        else:
            await ctx.send("User not in vc")

    @commands.command(pass_context=True)
    async def leave(self, ctx):
        if ctx.voice_client:
            await ctx.guild.voice_client.disconnect()
        else:
            await ctx.send("Not in a voice channel")

    @commands.command(pass_context=True, aliases=['p'])
    async def play(self, ctx, url: str):
        if ctx.author.voice:
            channel = ctx.author.voice.channel
            try:
                await channel.connect(reconnect=True, timeout=600)
                await ctx.guild.change_voice_state(channel=channel, self_deaf=True)
            except discord.ClientException:
                await ctx.guild.voice_client.move_to(channel)
        else:
            await ctx.send("User not in vc")
        search = ctx.message.content.replace("\\p ", '', 1)
        await ctx.message.delete()
        await ctx.trigger_typing()

        self.ydl_opts = {
            'format': 'bestaudio',
            'logger': Logger(),
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192'
            }]
        }
        match url:
            case url if "youtube" in url.lower():
                with ytdl.YoutubeDL(self.ydl_opts) as ydl:
                    info = ydl.extract_info(url, download=True)
                    self.Queue.append(ydl.prepare_filename(info).rsplit(".", 1)[0] + ".mp3")
                    await ctx.send("Queued " + ydl.prepare_filename(info).rsplit("[", 1)[0], delete_after=30)

                if not ctx.guild.voice_client.is_playing():
                    ctx.guild.voice_client.play(FFmpegPCMAudio(self.Queue[0]),
                                                after=lambda x: self.check_queue(voice_client=ctx.guild.voice_client,
                                                                                 from_play=True))
            case url if "open.spotify" in url.lower():
                # Small test of spotify functions
                match url:
                    case url if "/track/" in url.lower():
                        song = from_spotify_url(url)
                        with ytdl.YoutubeDL(self.ydl_opts) as ydl:
                            info = ydl.extract_info(song.youtube_link, download=True)
                            self.Queue.append(ydl.prepare_filename(info).rsplit(".", 1)[0] + ".mp3")
                            await ctx.send("Queued " + ydl.prepare_filename(info).rsplit("[", 1)[0], delete_after=30)

                        if not ctx.guild.voice_client.is_playing():
                            ctx.guild.voice_client.play(FFmpegPCMAudio(self.Queue[0]),
                                                        after=lambda x: self.check_queue(
                                                            voice_client=ctx.guild.voice_client,
                                                            from_play=True))

                    case url if "/album/" in url.lower():
                        await ctx.send("In development")
                        #songs = from_album(album_url=url)
                        #for song in songs:
                        #    with ytdl.YoutubeDL(self.ydl_opts) as ydl:
                        #        info = ydl.extract_info(song.youtube_link, download=False)
                        #        self.Queue.append([ydl.prepare_filename(info).rsplit(".", 1)[0] + ".mp3", song.youtube_link])
                        #        print(song.youtube_link)
                        #with ytdl.YoutubeDL(self.ydl_opts) as ydl:
                        #    if not ctx.guild.voice_client.is_playing():
                        #        await asyncio.wait_for(self.download([self.Queue[0][1]]), 120)
                        #        ctx.guild.voice_client.play(FFmpegPCMAudio(self.Queue[0][0]),
                        #                                    after=lambda x: self.check_queue(
                        #                                    voice_client=ctx.guild.voice_client, from_play=True))
                        #        await asyncio.wait_for(self.download([self.Queue[1][1]]), 120)
            case _:
                with ytdl.YoutubeDL(self.ydl_opts) as ydl:
                    info = ydl.extract_info("https://youtube.com" +
                                            YoutubeSearch(search,max_results=1).to_dict()[0]['url_suffix'],
                                            download=True)
                    self.Queue.append(ydl.prepare_filename(info).rsplit(".", 1)[0] + ".mp3")
                    await ctx.send("Queued " + ydl.prepare_filename(info).rsplit("[", 1)[0], delete_after=30)

                if not ctx.guild.voice_client.is_playing():
                    ctx.guild.voice_client.play(FFmpegPCMAudio(self.Queue[0]),
                                                after=lambda x: self.check_queue(voice_client=ctx.guild.voice_client,
                                                                                 from_play=True))

    async def download(self, url):
        with ytdl.YoutubeDL(self.ydl_opts) as ydl:
            ydl.download(url)

    @commands.command(pass_context=True, aliases=['q'])
    async def queue(self, ctx):
        await ctx.message.delete()
        tempQ = []
        for song in self.Queue:
            tempQ.append(song.rsplit("[", 1)[0])
        embed = discord.Embed(
            title="Queue",
            colour=discord.Colour.from_rgb(214, 34, 73)
                              )
        if len(self.Queue) > 20:
            Qshort = tempQ[0:19]
            embed.add_field(name="Queue", value="\n".join(Qshort))
            embed.add_field(name=" ", value="And " + str(len(self.Queue) - 20) + " more", inline=True)
        elif len(self.Queue) is not None:
            embed.add_field(name="Queue", value="\n".join(tempQ), inline=True)
        await ctx.send(embed=embed, delete_after=60)


    @commands.command(pass_context=True, aliases=['s'])
    async def skip(self, ctx):
        if ctx.guild.voice_client.is_playing():
            ctx.guild.voice_client.pause()
            self.check_queue(voice_client=ctx.guild.voice_client)

    @commands.command(pass_context=True)
    async def resume(self, ctx):
        if ctx.guild.voice_client.is_paused():
            ctx.guild.voice_client.resume()

    @commands.command(pass_context=True)
    async def pause(self, ctx):
        if not ctx.guild.voice_client.is_paused():
            ctx.guild.voice_client.pause()
        elif ctx.guild.voice_client.is_paused():
            ctx.guild.voice_client.resume()

    @commands.command(pass_context=True, aliases=['clear'])
    async def stop(self, ctx):
        self.Queue = []
        ctx.guild.voice_client.stop()

    def check_queue(self, voice_client: discord.VoiceClient = None, from_play=False):
        match self.Queue:
            case self.Queue if len(self.Queue) > 1:
                print("Queue isn't empty")
                if not len(self.Queue[0][0]) == 1:
                    voice_client.play(FFmpegPCMAudio(self.Queue[1][0]),
                                      after=lambda x: self.check_queue(voice_client=voice_client))
                    asyncio.wait_for(self.download(self.Queue[2][1]), 120)
                else:
                    voice_client.play(FFmpegPCMAudio(self.Queue[1]),
                                                     after=lambda x: self.check_queue(voice_client=voice_client))
                if len(self.Queue[0][0]) == 1:
                    os.remove(self.Queue[0])
                else:
                    os.remove(self.Queue[0][0])
                self.Queue.pop(0)

            case self.Queue if len(self.Queue) == 1:
                print("Queue is empty")
                os.remove(self.Queue[0])
                self.Queue = []
            case _:
                print("Unknown error!")

    def init_spotify(self):
        return SpotifyClient.init(client_id=json.load(open("api.json"))["spotify_id"],
                                  client_secret=json.load(open("api.json"))["spotify_secret"], user_auth=False)

def setup(bot):
    bot.add_cog(Commands(bot))

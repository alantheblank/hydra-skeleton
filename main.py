import discord
from discord.ext import commands
import json
import asyncio


client = commands.Bot(max_message=1000000, status=discord.Status.dnd, command_prefix="\\", case_insensitive=True)



@client.event
async def on_ready():
    await client.change_presence(activity=discord.Game(name="Praxis"))
    print("Ready")

client.load_extension("commands")
client.run(json.load(open("api.json"))["api"])

